const express = require('express');
const path = require('path');
const app = express();

// ------ Set a static folder--------
app.use(express.static(path.join(__dirname, 'public')))

// ------ Set default route website--------
app.get('/', (req, res) => {
    return res.sendFile( path.join(__dirname, 'public', 'index.html'));
});

app.get('/contact', (req, res) => {
    return res.sendFile( path.join(__dirname, 'public', 'contact.html'));
});

app.get('/about', (req, res) => {
    return res.sendFile( path.join(__dirname, 'public', 'about.html'));
});

app.get('/home', (req, res) => {
    return res.sendFile( path.join(__dirname, 'public', 'home.html'));
});

// API
app.post('/v1/contact', (req, res) => {
    res.send('<h2>Thank you! Your submission was successful! 😃</h2>');
    
});


const PORT = process.env.PORT || 8000
app.listen(PORT, () => console.log(`server is started on ${PORT}`));